# Atelier de traduction collective de _Quels problèmes les artistes éditeurices peuvent-iels résoudre ?_

## Description
Ceci est une fork du projet https://gitlab.com/yanntrividic/the-moral-of-the-xerox-vf pour être utilisé sur _Quels problèmes_. Pour plus d'infos, consulter le dépôt original.

## License
Traduction : CC BY-NC-SA 4.0 \
Texte original : Clara Balaguer et Florian Cramer \
Infrastructure : GNU-GPL3 \
Custom pagedjs hooks : GNU-GPL3