/**
 * @file Pagedjs' hook that adds an overlay to pagedjs to display content that's not in the print.
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 *
 * based on Benoit Launay's forensic.js script
 * @see https://gitlab.coko.foundation/pagedjs/templaters/forensic
 */

class overlay extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        console.log("overlay is working...");
        let pannel = document.createElement("aside");
        pannel.setAttribute("data-id", "overlay")

        pannel.style = "position: fixed;"; // otherwise pagedjs removes the position attribute
        generateOverlayTags(pannel);
        document.querySelector("body").appendChild(pannel);
        var modal = this.generateModalAndButton(pannel);
        if(this.isFirstVisit()){ // put a ! if you want to debug the modal
            this.displayModal(modal);
        }
    }

    isFirstVisit(){
        return localStorage.getItem("first-visit") == null ;
    }

    displayModal(modal){
        console.log("displayModal")
        modal.style.display = "block";
        localStorage.setItem("first-visit", "false");
    }

    generateModalAndButton(pannel){
        var btnInfo = document.createElement("button");
        btnInfo.setAttribute("id", "infoButton");
        btnInfo.innerText = "Infos";
        btnInfo.onclick = function() {
            modal.style.display = "block";
        }

        var btnSave = document.createElement("button");
        btnSave.setAttribute("id", "saveButton");
        btnSave.innerText = "Sauvegarder";
        btnSave.onclick = function() {
            savePads();
        }

        var modal = document.createElement("div");
        modal.setAttribute("data-id", "modal")
        modal.style = "position: fixed; display:none;"; // otherwise pagedjs removes the position attribute, and we don't want it shown by default

        document.querySelector("body").appendChild(modal);

        var modalContent = document.createElement("div");
        modalContent.setAttribute("class", "modalContent");
        modal.appendChild(modalContent);

        var close = document.createElement("span");
        close.setAttribute("class", "close");
        close.innerHTML = "&times;"
        close.onclick = function() {
            modal.style.display = "none";
        }
        modalContent.appendChild(close);
        
        window.onclick = function(event) {
            if (event.target == modal) {
            modal.style.display = "none";
            }
        }

        var text = document.createElement("p");
        text.innerHTML = `<p><p>Cette page web est un outil conçu pour traduire collectivement et simplement ce texte, tout en l’éditant et en le mettant en page. Cliquez sur les liens du menu pour modifier son contenu&#x202F;!</p><p>Code source <a href="https://gitlab.com/yanntrividic/ateliers-quels-problemes">ici</a>.`;
        modalContent.appendChild(text);

        pannel.appendChild(btnInfo);
        pannel.appendChild(document.createElement("br"));
        pannel.appendChild(btnSave);
        pannel.appendChild(document.createElement("br"));
        pannel.appendChild(document.createElement("br"));
        return modal;
    }
}
Paged.registerHandlers(overlay);