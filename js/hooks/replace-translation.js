class replaceTranslation extends Paged.Handler {
    // this let us call the methods from the the chunker, the polisher and the caller for the rest of the script
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    
    beforeParsed(content) {
        console.log("replaceTranslation is working...");
        
        const sources = content.querySelectorAll(".source"); // all the text we must translate
        sources.forEach((source) => {
            var target = this.getTarget(source);
            if (target) { // if the following element is a target element
                if (target.innerText != ""){ // if a translation is not empty
                    source.remove();
                } else target.remove();
            }
        });
    }

    getTarget(source){
        let target = source.nextSibling.nextSibling; // all the translations
        if(target.classList.contains("target")){
            return target;
        } else {
            console.error("Translation element is missing after " + source.innerText.slice(0, 10) + "...");
            return false;
        }
    }
}

Paged.registerHandlers(replaceTranslation);